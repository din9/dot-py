import requests
from bs4 import BeautifulSoup
import csv

# res = requests.get('https://learncodeonline.in')
# soup = bs4.BeautifulSoup(res.text, 'lxml')
# hi = soup.select('html') 
#
# print(hi[0].getText())
# print(type(soup))
# print(res.text)



# article = soup.find(class_='entry-header')
# print(article.prettify())
# article = soup.find('span', class_='article')
# print(soup.prettify())

# ****
source = requests.get('https://coreyms.com').text
soup = BeautifulSoup(source, 'lxml')
# ****


csv_file = open('cms_scrape.csv', 'w')
csv_writer = csv.writer(csv_file)
csv_writer.writerow(['headline', 'summary', 'video_link'])



# *** Lo0p _______________________________________________
for article in soup.find_all('article'):
    headline = article.h2.a.text
    print(headline)
    
    summary = article.find('div', class_="entry-content").p.text
    print(summary)

    try:
        iframe = soup.find('iframe')['src']
        iframeid = iframe.split('/')[4]
        iframesrc = iframeid.split('?')[0]
        yt_link = f'https://youtube.com/watch?v={iframesrc}'

    except Exception as e:
        yt_link = None


    print(yt_link)
    print()

    csv_writer.writerow([headline, summary, yt_link])
csv_file.close()

    # article = soup.find('article')
    # print(article)



# *** SiNGLE _______________________________________________ 
# iframe = soup.find('iframe')['src']
# for iframe in soup.find_all('iframe')['src']:
#     iframeid = iframe.split('/')[4]
#     iframesrc = iframeid.split('?')[0]
#
#
#     yt_link = f'https://youtube.com/watch?v={iframesrc}'
#     print(yt_link)
#

