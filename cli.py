import click

@click.command()
@click.option('--name', '-n', default='John', help='Firstname description')
@click.option('--salary', '-s', nargs=2, help='Your Montly Salary', type=int)
@click.option('--location', '-l', help='Places You ve Visited', multiple=True)


def main(name, salary, location):
    click.echo('Hello World {}, My Salary is {}'.format(name,sum(salary)))
    click.echo('\n'.join(location))

if __name__ == '__main__':
    main()


# for group = https://www.youtube.com/watch?v=gLCfLOaIHoQ&pp=QABIAA%3D%3D
