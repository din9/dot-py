import sqlite3
from Dbconnect2 import DBConnect


def main():
    dbconnect = DBConnect()

    while True:
        IndexOp = int(input("Select Operation, \n1- Add:\n2- List Admins \n3- Update Record \n4- Delete Record \n0- Exit \nChoose one: "))

        if(IndexOp == 0):
            break;

        if(IndexOp == 1):
            Name = input("Enter name: ")
            Age = int(input("Enter Age: "))
            dbconnect.Add(Name, Age)

        elif(IndexOp == 2):
            dbconnect.ListAdmins()

        elif(IndexOp == 3):
            Name = input("Enter name: ")
            Age = int(input("Enter Age: "))
            dbconnect.UpdateRecord(Age, Age)

        elif(IndexOp == 4):
            Name = input("Enter name to delete: ")
            dbconnect.DeleteRecord(Name)


    # db=sqlite3.connect("information.db")
    # db.row_factory = sqlite3.Row
    # db.execute("create table if not exists Admin(Name text, Age int)")
    # #Add records
    # db.execute("insert into Admin(Name, Age) values(?,?)", ("Hussien", 28))
    # db.execute("insert into Admin(Name, Age) values(?,?)", ("Jena", 22))
    # db.execute("insert into Admin(Name, Age) values(?,?)", ("Laya", 21))
    # db.commit()


if __name__ == '__main__':main()



