from tkinter import *
from tkinter import ttk
from tkinter import messagebox

# GRID
# root = Tk()
# style = ttk.Style()
# style.theme_use('classic')
#
# ttk.Label(root, text="Green", background="Green").grid(row=0, column=0, padx=5, pady=5, sticky='snew')
# ttk.Label(root, text="Yellow", background="Yellow").grid(row=0, column=1, ipadx=10, ipady=10, sticky='snew')
# ttk.Label(root, text="Blue", background="Blue")\
#         .grid(row=0, column=2, rowspan=2, sticky='snew')
# ttk.Label(root, text="Orange", background="Orange")\
#         .grid(row=1, column=0, columnspan=2, sticky='snew', ipadx=20, ipady=10)
#
#
# root.rowconfigure(0, weight=2)
# root.rowconfigure(1, weight=1)
# root.columnconfigure(1, weight=2)
# root.columnconfigure(2, weight=1)


# FRAME 
# root = Tk()
# frame1 = ttk.Frame(root)
# frame1.pack()
# frame1.config(height=200, width=200, relief=RIDGE, padding=(10, 10))
# ttk.Button(frame1, text="Click me frame 1").grid(row=0, column=0)
# ttk.Button(frame1, text="Click me frame 2").grid(row=0, column=3)
#
# frame1 = ttk.Frame(root)
# frame1.pack()
# frame1.config(height=200, width=200, relief=RIDGE, padding=(10, 10))
# ttk.Button(frame1, text="Click me frame 1").pack()
# ttk.Button(frame1, text="Click me frame 2").pack()
#
# ttk.LabelFrame(height=100, width=100, text="Thrid frame").pack()


root = Tk()
root.title("Title Aplikasi")
root.resizable(True, True)
# root.resizable(False, False)


label1 = ttk.Label(root, text="Username: ")
label1.grid(row=0, column=0)
inputUserName = ttk.Entry(root, width=20)
inputUserName.grid(row=0, column=1)
label2 = ttk.Label(root, text="Password: ")
label2.grid(row=1, column=0)
inputPassword = ttk.Entry(root, width=20)
inputPassword.grid(row=1, column=1)
inputPassword.config(show="*")
# ButtonLogin = ttk.Button(root, text="Login", command=lambda: BuClick()) OR
ButtonLogin = ttk.Button(root, text="Login")
ButtonLogin.grid(row=2, column=1)


def BuClick():
    print("Username: {}, Password: {}".format(inputUserName.get(), inputPassword.get()))
    if(inputUserName.get() == "syam" and inputPassword.get() == "123"):
        messagebox.showinfo(title="Login Info", message="Username: {}, Password: {}".format(inputUserName.get(), inputPassword.get()))

    else:
        messagebox.showinfo(title="Login Info", message="Authentication Salah")


ButtonLogin.config(command=BuClick)


root.mainloop()
