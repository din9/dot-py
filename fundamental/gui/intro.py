from tkinter import *
from tkinter import ttk 



# === Button And Entry ===
root = Tk()
entry = ttk.Entry(root, width=30)
entry.pack()
button = ttk.Button(root, text="Click Me!")
button.pack()
def BuClick():
    print(entry.get())
    entry.delete(0, END)
    # entry.insert(0, END)
button.config(command=BuClick)


# === Call Function ===
# def BuClick(id):
#     print("Button is clicked {}: ".format(id))
#
# callfunction = Tk()
# bu = ttk.Button(callfunction, text="Click me 1", command=lambda: BuClick(10)).pack()
# # bu.pack()
#
# callfunction.mainloop()


# === Event ===
# event = Tk()
# def key_press(event):
#     print("type: {}".format(event.type))
#
# def button_press(event):
#     print("Button is press")
#
# bu=ttk.Button(event, text="Click me")
# bu.pack()
# bu.bind("<Button-1>", button_press)
#
# event.bind("<Control-c>", key_press)
# event.mainloop()

