from tkinter import *
from tkinter import ttk 
from tkinter import messagebox
from sql import DBConnect 


class ListTickets:
    def __init__(self):
        self._dbConnect = DBConnect()
        self._root = Tk()
        tv = ttk.Treeview(self._root)
        tv.pack()
        tv.heading("#0", text="ID")
        tv.configure(column=('fullname', 'gender', 'comment'))
        tv.heading("fullname", text="fullname")
        tv.heading("gender", text="gender")
        tv.heading("comment", text="comment")
        cursor = self._dbConnect.ListTickets()
        for row in cursor:
            tv.insert('', 'end', '#{}'.format(row["ID"]), text=row["ID"])
            tv.set('#{}'.format(row["ID"]), 'fullname', row["fullname"])
            tv.set('#{}'.format(row["ID"]), 'gender', row["gender"])
            tv.set('#{}'.format(row["ID"]), 'comment', row["comment"])
            # print("ID: {}, Fullname: {}, Gender: {}, Comment: {}".format(row["ID"], row["fullname"], row["gender"], row["comment"]))

        self._root.mainloop()
