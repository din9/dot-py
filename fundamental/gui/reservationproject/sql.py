import sqlite3

class DBConnect:
    def __init__(self):
        self._db = sqlite3.connect("Reservation.db")
        self._db.row_factory = sqlite3.Row
        self._db.execute("create table if not exists Ticket(ID integer primary key autoincrement, fullname text, gender text, comment text)")
        self._db.commit()


    def Add(self,fullname, gender, comment):
        self._db.row_factory = sqlite3.Row
        self._db.execute("insert into Ticket(fullname, gender, comment) values(?,?,?)",(fullname, gender,comment))
        self._db.commit()
        return ("Record is Added")

    def ListTickets(self):
        cursor = self._db.execute("select * from Ticket")
        return cursor


    def DeleteRecord(self, ID):
        self._db.row_factory = sqlite3.Row
        self._db.execute("delete from Ticket where ID={}".format(ID))
        self._db.commit()
        return ("Record is deleted")

    def UpdateRecord(self, ID, comment):
        self._db.row_factory = sqlite3.Row
        self._db.execute("update Ticket set Comment=? where ID=?", (comment, ID))
        self._db.commit()
        return ("Record is Updated")




