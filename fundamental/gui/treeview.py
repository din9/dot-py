from tkinter import *
from tkinter import ttk

root = Tk()

tv = ttk.Treeview()
tv.pack()

# '' = super
# '' = ntah
# '' = name rujukan
# text="" = name

tv.insert('', '0', 'item1', text='Hussein')
tv.insert('', '1', 'item2', text='Jena')
tv.insert('', '0', 'item3', text='Laya')
tv.insert('item1', '0', 'item4', text='Rana')


# '' = nama rujukan
# '' = dalam super apa
# '' = ntah

tv.move('item2', 'item1', '0')
tv.move('item3', 'item1', 'end')

tv.item('item1', open=True)


root.mainloop()
