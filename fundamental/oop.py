# ++++++++++++++++++++++++++++USING CLASS
# class kereta():
#     def Setbentuk(self, bentukA, bentukB):
#         self._bentuka = bentukA
#         self._bentukb = bentukB
#
#     def SetWarna(self, warnaA, warnaB):
#         self._warnaA = warnaA
#         self._warnaB = warnaB
#
#     def Getbentuk(self):
#        return self._bentuka + self._bentukb
#
#
#
# def main():
#     print("Belajar OOP")
#     oop1 = kereta()
#     oop1.Setbentuk(2,3)
#     jumlah = oop1.Getbentuk()
#     print(jumlah)
#
#
# if __name__ == "__main__":
#     main()


# ++++++++++++++++++++++++USING CONTRUCTOR
# class kereta():
#     def __init__(self, bentukA, bentukB):
#         self.bentuka = bentukA 
#         self.bentukb = bentukB 
#
#     def getBentuk(self):
#         return self.bentuka + self.bentukb
#
#
# def main():
#     oop1 = kereta(3,4)
#     jumlah = oop1.getBentuk()
#     print(jumlah)
#
#
# if __name__ == "__main__":
#     main()


# ++++++++++++++++++++++++USING kwargs
# class kereta:
#     def __init__(self, **kwargs):
#         self._Data = kwargs
#
#     def GetType(self):
#         return self._Data["Type"]
#     
#     def GetModel(self):
#         return self._Data["Model"]
#
#     def GetPrice(self):
#         return self._Data["Price"]
#
#     def GetMilesDrive(self):
#         return self._Data["MilesDrive"]
#
#     def GetOwner(self):
#         return self._Data["Owner"]
#
#
#
# def main():
#     myCar = kereta(Type="BMW", Model="2015", Price="26000")
#     CurrentPrice = myCar.GetPrice()
#     print("harga: {}".format(CurrentPrice))
#
#     aliCar = kereta(Price="8000")
#     CurrentPrice1 = aliCar.GetPrice()
#     Jumlahsebenar = int(CurrentPrice1) + 2000
#     print("harga untuk ali: {}".format(Jumlahsebenar))
#
#
# if __name__ == "__main__": 
#     main()

class Operations:
    def sum(self, n1, n2):
        return n1+n2

    def div(self, n1, n2):
        return n1/n2


class MultiOperations(Operations):
    def multi(self, n1, n2):
        return n1 * n2

    # Untuk panggil function dkat luar
    # def sum(self, n1, n2):
    #     return super().sum(n1,n2)

    # Untuk panggil function dkat dlm 
    def sum(self, n1, n2):
        return (n1 + n2)



