from tkinter import *
import tkinter.messagebox as MessageBox
import os

root = Tk()



def message():
    os.chdir('/home/syam/Pictures')
    path = os.walk('/home/syam/Pictures')
    MessageBox.showinfo("Testing", path)
    print(path)

    



#Label ---------------------------------
l1 = Label(root, text="Title")
l1.grid(row=0, column=0)

l1 = Label(root, text="Author")
l1.grid(row=0, column=2)

l1 = Label(root, text="Year")
l1.grid(row=1, column=0)

l1 = Label(root, text="ISBN")
l1.grid(row=1, column=2)


#Input ---------------------------------
title_text = StringVar()
el = Entry(root, textvariable=title_text)
el.grid(row=0, column=1)

author_text= StringVar()
el = Entry(root, textvariable=author_text)
el.grid(row=0, column=3)


year_text = StringVar()
el = Entry(root, textvariable=year_text)
el.grid(row=1, column=1)

isbn_text = StringVar()
el = Entry(root, textvariable=isbn_text)
el.grid(row=1, column=3)



#Listbox ---------------------------------
list1 = Listbox(root, height=6, width=35)
list1.grid(row=2, column=0, rowspan=6, columnspan=2)


#Scrollbar ---------------------------------
sb1 = Scrollbar(root)
sb1.grid(row=2, column=2, rowspan=6)

list1.configure(yscrollcommand=sb1.set)
sb1.configure(command=list1.yview)


#Scrollbar ---------------------------------
b1 = Button(root, text="View All", width=12, command=message)
b1.grid(row=2, column=3)

b1 = Button(root, text="Search Entry", width=12)
b1.grid(row=3, column=3)

b1 = Button(root, text="Add Entry", width=12)
b1.grid(row=4, column=3)

b1 = Button(root, text="Update selected", width=12)
b1.grid(row=5, column=3)

b1 = Button(root, text="Deleted selected", width=12)
b1.grid(row=6, column=3)

b1 = Button(root, text="Close", width=12)
b1.grid(row=7, column=3)

root.mainloop()
