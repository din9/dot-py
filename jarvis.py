import pyttsx3
import os
import datetime
import speech_recognition as sr
import wikipedia
import smtplib
import webbrowser as wb
import psutil
import pyautogui
import random

import json
import requests
from urllib.request import urlopen
import wolframalpha
import time 



engine = pyttsx3.init()
wolframalpha_app_id = 'wolframa alpha id will go here'


def speak(audio):
    engine.say(audio)
    engine.runAndWait()

def masa():
    Time = datetime.datetime.now().strftime("%H:%M:%S")
    speak("what's is your name")
    speak(Time)


def date(): 
    year = datetime.datetime.now().year
    month = datetime.datetime.now().month
    date = datetime.datetime.now().day
    speak("The current date is")
    speak(date)
    speak(month)
    speak(year)


def wishme():
    speak("program")
    masa()
    date()
    hour = datetime.datetime.now().hour
    # print(hour)

    if hour>=6 and hour<12:
        speak("Good morning Sir!")
    elif hour>=12 and hour<18:
        speak("Good afternoon Sir!")
    elif hour>=18 and hour<24:
        speak("Good evening Sir!")
    else:
        speak("Good night Sir!")

    speak("Jarvis is your service. Please tell me how can I help you today")


def TakeCommand():
    r = sr.Recognizer() 
    with sr.Microphone() as source:
        print("listening...")
        r.pause_threshold = 1
        audio = r.listen(source)

    try:
        print("Recognizing...")
        query = r.recognize_google(audio, language='en-US')
        # test(query)
        print(query)

    except Exception as e:
        print(e)
        print("Say that again please...")
        return "None"

    return query



def sendEmail(to, content):
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    
    server.login('username@gmail.com','password')
    server.sendmail('usernamee@gmail.com',to,content)
    server.close()


def cpu():
    usage = str(psutil.cpu_percent())
    speak('CPU is at' + usage)
    battery = psutil.sensors_battery()
    speak('Battery is at')
    speak(battery.percent)


# def screenshot():
#     img = pyautogui.screenshot()
#     img.save('C:\Users\calls\Desktop\screenshot.png')








if __name__ == "__main__":
    wishme()

    while True:
        query = TakeCommand().lower()
        if 'time' in query:
            masa()
        elif 'date' in query:
            date()
        elif 'python' in query:
            speak("Searching...")
            # query = query.replace('wikipedia','')
            result = wikipedia.summary(query, sentences=1)
            speak('According to Wikipedia')
            print(result)
            speak(result)

        elif 'send email' in query:
            try:
                speak('What should I say?')
                content=TakeCommand()

                speak('Who is the Receiver?')
                receiver = input("Enter Receive's Email: ")
                to = receiver
                sendEmail(to, content)
                speak(content)
                speak('Email is has been send.')

            except Exception as e:
                print(e)
                print('Unable to send')

        elif 'search in chrome' in query:
            speak('What should I search?')
            chromepath = 'C:\Program Files\Google\Chrome\Application\chrome.exe %s'
            search = TakeCommand().lower()
            wb.get(chromepath).open_new_tab(search+'.com')

        elif 'search youtube' in query:
            speak('What should I search')
            search_Term = TakeCommand().lower()
            speak("Here We go to YOUTUBE!")
            wb.open('https://www.youtube.com/results?search_query='+search_Term)

        elif 'google' in query:
            speak('What should I search')
            search_Term = TakeCommand().lower()
            speak("Searching..")
            wb.open('https://www.google.com/search?q='+search_Term)


        elif 'cpu' in query:
            cpu()

        elif 'exit' in query:
            speak('yes, thank you')
            quit()

        elif 'word' in query:
            speak('Opening MSWord...')
            ms_word = r'C:\laragon\laragon.exe'
            os.startfile(ms_word)

        elif 'write a note' in query:
            speak('What should I write, Sir?')
            notes = TakeCommand()
            file = open('notes.txt', 'w')
            speak('Sir should I Include Date and Time?')
            ans = TakeCommand()
            if 'yes' in ans or 'sure' in ans:
                strTime = datetime.datetime.now().strftime("%H:%M:%S")
                file.write(strTime)
                file.write(":-")
                file.write(notes)
                speak('Done Taking Notes, SIR!')
            else:
                file.write(notes)

        elif 'show note' in query:
            speak('showing notes')
            file = open('notes.txt', 'r')
            print(file.read())
            speak(file.read())

        # elif 'screenshot' in query:
        #     screenshot()

        elif 'play music' in query:
            song_dir = 'D:\mp3'
            music = os.listdir(songs_dir)
            speak('What should I Play?')
            ans = TakeCommand().lower()
            
            while('number' not in ans and ans != 'random' and ans != 'you choose'):
                speak('I could not understand you. Please try again!')
                ans = TakeCommand().lower()
            
            if 'number' in ans:
                no = int(ans.replace('number', ''))
            if 'random' or 'you choose' in ans:
                no = random.randint(1,4)

            os.startfile(os.path.join(song_dir, music[no]))

        elif 'remember that' in query:
            speak("What should I Remember?")
            memory = TakeCommand()
            speak("You asked me to remember that" + memory)
            remember = open('memory.text', 'w')
            remember = write(memory)
            remember.close()

        elif 'do you remember anything' in query:
            remember = open('memory.text','r')
            speak("You asked me to remember that" + remember.read())

        elif 'news' in query:
            try:
                jsonObj = urlopen("http://newsapi.org/v2/everything?q=wallet&from=2020-09-15&sortBy=publishedAt&apiKey=140408f3a000463ab6f061aa3432cdc7")
                data = json.load(jsonObj)
                i=1
                speak("Here are some top headline from published Industry")
                print("=================TOP HEADLINE==================")
                for item in data['articles']:
                    print(str(i)+'. '+item['title']+'\n')
                    print(item['description']+'\n')
                    speak(item['title'])
                    i += 1 

            except Exception as e:
                print(str(e))

        elif 'where is' in query:
            query = query.replace("Where is","")
            location = query
            speak("User asked" + location)
            wb.open_new_tab("https://www.google.com/maps/place/"+location)


        elif 'calculate' in query:
            client = wolframalpha.Client(wolframalpha_app_id)
            indx = query.lower().split().index('calculate')
            query = query.split()[indx + 1:]
            res = client.query(''.join(query))
            answer = next(res.results).text
            print('The answer is : '+answer)
            speak('The answer is : '+answer)
        

        elif 'what is' in query or 'who is' in query:
            client = wolframalpha.Client(wolframalpha_app_id)
            res = client.query(query)

            try:
                print(next(res.results).text)
                speak(next(res.results).text)

            except StopIteration:
                print("No Result")

        elif 'stop listening' in query:
            speak('For How many second you want me to stop listening to your command')
            ans = int(TakeCommand())
            time.sleep(ans)
            print(ans)


        elif 'log out' in query:
            os.system("shutdown -l")

        elif 'restart' in query:
            os.system("shutdown /r /t /1")

        elif 'shutdown' in query:
            os.system("shutdown /s /t")










# wishme()
