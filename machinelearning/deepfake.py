import cv2
import numpy as np
import dlib 

frontal_face_detector = dlib.get_frontal_face_detector()
frontal_face_predictor = dlib.shape_predictor("../../../data/shape_predictor_68_face_landmarks.dat")

source_image = cv2.imread("./try.jpg")
source_image_copy = source_image
source_image_grayscale = cv2.cvtColor(source_image, cv2.COLOR_BGR2GRAY)

destination_image  = cv2.imread("./try.jpg")
destination_image_copy = destination_image
destination_image_grayscale = cv2.cvtColor(destination_image, cv2.COLOR_BGR2GRAY)
cv2.imshow("destination_image", destination_image_grayscale)
