import sys
import nmap

target = str(sys.argv[1])
ports = [21,22,80,139,443,8080,9090]

scan_v = nmap.PortScanner()

print("\nScanning", target, "for ports 21,22,80,139,443 and 8080...\n")

for port in ports:
    portscan = scan_v.scan(target, str(port))
    print("Port",port," is ", portscan['scan'][target]['tcp'][port]['state'])


print("\nHost", target, " is ", portscan['scan'][target]['status']['state'])


# 21 – FTP
# 22 – SSH
# 25 – SMTP (sending email)
# 53 – DNS (domain name service)
# 80 – HTTP (web server)
# 110 – POP3 (email inbox)
# 123 – NTP (Network Time Protocol)
# 143 – IMAP (email inbox)
# 443 – HTTPS (secure web server)
# 465 – SMTPS (send secure email)
# 631 – CUPS (print server)
# 993 – IMAPS (secure email inbox)
# 995 – POP3 (secure email inbox)

